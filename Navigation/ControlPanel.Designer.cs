﻿namespace Navigation
{
    partial class ControlPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.webCamFeed = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.depthCamFeed = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.detectedObjects = new System.Windows.Forms.ListView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.webCamFeed)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.depthCamFeed)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.webCamFeed);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(336, 268);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Webcam - Forward";            
            // 
            // webCamFeed
            // 
            this.webCamFeed.Location = new System.Drawing.Point(7, 20);
            this.webCamFeed.Name = "webCamFeed";
            this.webCamFeed.Size = new System.Drawing.Size(320, 240);
            this.webCamFeed.TabIndex = 0;
            this.webCamFeed.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.depthCamFeed);
            this.groupBox2.Location = new System.Drawing.Point(354, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(336, 268);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Depthcam - Forward";
            // 
            // depthCamFeed
            // 
            this.depthCamFeed.Location = new System.Drawing.Point(6, 19);
            this.depthCamFeed.Name = "depthCamFeed";
            this.depthCamFeed.Size = new System.Drawing.Size(320, 240);
            this.depthCamFeed.TabIndex = 0;
            this.depthCamFeed.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.detectedObjects);
            this.groupBox3.Location = new System.Drawing.Point(12, 286);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(678, 319);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "DetectedObjects";
            // 
            // detectedObjects
            // 
            this.detectedObjects.Location = new System.Drawing.Point(7, 20);
            this.detectedObjects.Name = "detectedObjects";
            this.detectedObjects.Size = new System.Drawing.Size(661, 293);
            this.detectedObjects.TabIndex = 0;
            this.detectedObjects.UseCompatibleStateImageBehavior = false;
            // 
            // ControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 612);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ControlPanel";
            this.Text = "ControlPanel";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.webCamFeed)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.depthCamFeed)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox webCamFeed;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox depthCamFeed;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView detectedObjects;
    }
}