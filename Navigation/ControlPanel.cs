﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Navigation
{
    public partial class ControlPanel : Form
    {
        private Bitmap webCamFrame;
        private Bitmap depthCamFrame;
        public ControlPanel()
        {
            InitializeComponent();
            detectedObjects.View = View.Details;
            detectedObjects.Columns.Add("X", -2, HorizontalAlignment.Center);
            detectedObjects.Columns.Add("Y", -2, HorizontalAlignment.Center);
            detectedObjects.Columns.Add("Classification", -2, HorizontalAlignment.Center);            

        }
        public void DisplayClassifiedObjects(List<ClassifiedObject> objects)
        {
            detectedObjects.Items.Clear();
            for(int i = 0; i < objects.Count; i++)
            {
                
                var item = new ListViewItem(new[] { objects[i].X.ToString(), objects[i].Y.ToString(), objects[i].Classification.ToString() });                
                detectedObjects.Items.Add(item);
            }
        }
        public Bitmap DepthCamFrame
        {
            get { return this.depthCamFrame; }
            set
            {
                this.depthCamFrame = value;
                Image toDispose = this.depthCamFeed.Image;
                this.depthCamFeed.Image = value;
                if(toDispose != null)
                {
                    toDispose.Dispose();
                }
            }
        }
        public Bitmap WebCamFrame
        {
            get { return this.webCamFrame; }
            set
            {               
                this.webCamFrame = value;
                Image toDispose = this.webCamFeed.Image;
                this.webCamFeed.Image = value;
                if (toDispose != null)
                {
                    toDispose.Dispose();
                }

            }
        }        
    }
}
