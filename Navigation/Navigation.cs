//System Imports
using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Forms;

//Threads/Robotics Imports.
using Microsoft.Ccr.Core;
using Microsoft.Ccr.Adapters.WinForms;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using drive = Microsoft.Robotics.Services.Drive.Proxy;
using motor = Microsoft.Robotics.Services.Motor.Proxy;
using depthCam = Microsoft.Robotics.Services.DepthCamSensor.Proxy;
using webcam = Microsoft.Robotics.Services.WebCamSensor.Proxy;
using kinect = Microsoft.Robotics.Services.Sensors.Kinect;


using W3C.Soap;
using Navigation.Utilities;

namespace Navigation
{
    [Contract(Contract.Identifier)]
    [DisplayName("Navigation")]
    [Description("Navigation service (Navigation Service, navigates an agent towards a goal/goals by classifying depth-images.)")]    
    class NavigationService : DsspServiceBase
    {
        const int MAX_POWER = 1;
        const int DEFAULT_POLARITY = 1;
        const double TURN_POWER = 0.2;
        
        Classifier shapeClassifier = new Classifier();
        PotentialFields artificalPotentialField;
        ControlPanel panel;
        List<ClassifiedObject> classified;

        [ServiceState]
        NavigationState _state = new NavigationState();

        //Our Navigation Service.
        [ServicePort("/Navigation", AllowMultipleInstances = true)]
        NavigationOperations _mainPort = new NavigationOperations();

        #region Partners
        //Partner - Two wheel differential drive.
        [Partner("Drive", Contract = drive.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExisting)]
        drive.DriveOperations _drivePort = new drive.DriveOperations();
        drive.DriveOperations _drivePortNotify = new drive.DriveOperations();

        [Partner("DepthCam", Contract = depthCam.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate, Optional = true)]
        private depthCam.DepthCamSensorOperationsPort _depthCamSensorPort = new depthCam.DepthCamSensorOperationsPort();
        private depthCam.DepthCamSensorOperationsPort _depthCamSensorNotify = new depthCam.DepthCamSensorOperationsPort();

        [Partner("WebCam", Contract = webcam.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate, Optional = true)]
        private webcam.WebCamSensorOperations _webCamPort = new webcam.WebCamSensorOperations();
        private webcam.WebCamSensorOperations _webCamNotify = new webcam.WebCamSensorOperations();          
        #endregion

        public NavigationService(DsspServiceCreationPort creationPort)
            : base(creationPort){}
        
        protected override void Start()
        {
            WinFormsServicePort.Post(new RunForm(this.InitializeControlPanel));            
            artificalPotentialField = new PotentialFields();
            classified = new List<ClassifiedObject>();
                        
            _state.LogicalState = LogicalState.UnknownSurrondings;

            _drivePort.Subscribe(_drivePortNotify,typeof(drive.RotateDegrees));
            Activate(Arbiter.Receive<drive.RotateDegrees>(true, _drivePortNotify, TurningHandler));

            base.Start();            
            PerformTask();            
        }
        #region StateMachine        
        /// <summary>
        /// PerformTask - Method for the PerformTask state.
        /// </summary>
        private void PerformTask()
        {               
            while (_state.PerformingTask)
            {                
                switch(_state.LogicalState)
                {
                    case LogicalState.UnknownSurrondings:
                        MapSurrondings();
                        break;                    
                    case LogicalState.AdjustDirection:
                        AdjustDirection();
                        break;
                    case LogicalState.FreeForward:
                        SpawnIterator<int>(DEFAULT_POLARITY, AdjustVelocity);
                        return;
                    default:
                        break;
                }
            }
        }
        /// <summary>
        /// Method for the AdjustDirection state
        /// </summary>
        private void AdjustDirection()
        {
            _state.LogicalState = LogicalState.Busy;
            artificalPotentialField.ConstructFields(classified, new Point(0, 0));
            _state.HeadingOffset = (artificalPotentialField.potField.X + 5)* -1;
            LogError(_state.HeadingOffset.ToString());            
            SpawnIterator(Turn);
        }
        /// <summary>
        /// Method for the Mapping state.
        /// </summary>
        private void MapSurrondings()
        {
            _state.HeadingOffset = _state.CameraAngle;
            _state.LogicalState = LogicalState.Mapping;
            SpawnIterator(PollDepthFrame);            
        }
        #endregion
        #region DifferentialDriveFunctions
        /// <summary>
        /// Chooses an action after turning is finished, based on if the robot was mapping or just taking a turn.
        /// </summary>
        /// <param name="update">Updated Differential RotateDegrees state.</param>
        private void TurningHandler(drive.RotateDegrees update)
        {   
            if (update.Body.RotateDegreesStage.Equals(drive.DriveStage.Completed))
            {
                switch(_state.LogicalState)
                {
                    case LogicalState.Mapping:
                        _state.RotationCount++;
                        _state.LogicalState = LogicalState.UnknownSurrondings;
                        break;
                    case LogicalState.Busy:
                        _state.LogicalState = LogicalState.FreeForward;
                        SpawnIterator(PollColorFeed);            
                        break;
                    default:
                        break;
                }
            }
        }
        /// <summary>
        /// AdjustsVelocity - Decreases the robots velocity and changes the engine power levels per approach.
        /// </summary>
        /// <param name="polarity">Polarity For Forward/Backwards movement:
        ///                             +1  = Forward
        ///                             -1  = Backwards
        ///</param>
        private IEnumerator<ITask> AdjustVelocity(int polarity)
        {
            
            //Calculate decreasing velocity instead when approching object?
            double adjustedVelocity = 0.5;
            yield return Arbiter.Receive(
                false,
                StartMove(adjustedVelocity),
                delegate(bool result) { }
            );
        }

        /// <summary>
        /// Turn - Turns the robot in the direction specified.
        /// </summary>        
        /// <returns></returns>
        private IEnumerator<ITask> Turn(double power, int polarity)
        {
            yield return Arbiter.Receive(
                false,
                StartTurn(),
                delegate(bool result) { }
            );
        }

        /// <summary>
        /// StartMove - Request Forward/Backwards movement behaviour.
        /// </summary>
        /// <param name="power">Engine-power : [0,1] </param>
        /// <returns></returns>
        private Port<bool> StartMove(double power)
        {
            Port<bool> result = new Port<bool>();
            SpawnIterator<Port<bool>, double>(result, power, MoveStraight);
            return result;
        }

        /// <summary>
        /// StartTurn - Request Left/Right-turning behaviour.
        /// </summary>
        /// <param name="power">Engine-power : [0,1] </param>
        private Port<bool> StartTurn()
        {
            Port<bool> result = new Port<bool>();
            SpawnIterator(Turn);
            return result;
        }

        /// <summary>
        /// KillEngines - Request to kill the engines by setting their power to 0.
        /// </summary>        
        /// <returns></returns>
        private Port<bool> KillEngines()
        {
            Port<bool> result = new Port<bool>();
            SpawnIterator<Port<bool>, double>(result, 0, MoveStraight);
            return result;
        }


        /// <summary>
        /// Move Straight - Sets equal power to both motors.
        /// </summary>
        private IEnumerator<ITask> MoveStraight(Port<bool> done, double power)
        {
            drive.SetDrivePowerRequest setPower = new drive.SetDrivePowerRequest(power, power);
            yield return Arbiter.Choice(
                _drivePort.SetDrivePower(setPower),
                delegate(DefaultUpdateResponseType sucess) { done.Post(true); },
                delegate(W3C.Soap.Fault exception)
                {
                    LogError("Error : Failed to set the power for the drives.");
                    done.Post(false);
                }
            );

        }
        /// <summary>
        ///  Turn - Turns the robot in the direction enforced by the artificial potential field.
        /// </summary>        
        private IEnumerator<ITask> Turn()
        {
            
            bool successful = false;
            yield return Arbiter.Choice(
                _drivePort.RotateDegrees(_state.HeadingOffset, TURN_POWER),
                delegate(DefaultUpdateResponseType sucess) { successful = true; },
                delegate(W3C.Soap.Fault exception)
                {
                    LogError("Error : Failed to set the power for the drives.");
                }
            );            
            yield break;
        }
        #endregion
        #region Webcam
        /// <summary>
        /// Processes color images (not implement), it only displays the image.
        /// </summary>
        /// <param name="state"></param>
        private void processImage(Microsoft.Robotics.Services.WebCamSensor.WebCamSensorState state)
        {
            DisplayWebcamFrame(state.Bitmap);            
        }

        private IEnumerator<ITask> PollColorFeed()
        {
            yield return this._webCamPort.Get().Choice(                
                camResult => { processImage(camResult); },
                failure => { LogError(failure); }
                );

        }
        #endregion
        #region Depthcam        
        /// <summary>
        /// Sets each depth-point in the pixeldata into it's depth-value.
        /// </summary>        
        private short[] AdjustDepthPixels(short[] pixelData)
        {
            if (pixelData == null)
                return null;
            short[] depthData = new short[pixelData.Length];
            for (int i = 0; i < pixelData.Length; i++)
            {
                depthData[i] = (short)(((ushort)pixelData[i]) >> 3);
            }
            return depthData;
        }
        /// <summary>
        /// Processes and captures depth images by turning and classifying objects perceived.
        /// </summary>
        /// <param name="state"></param>
        private void processDepthImage(Microsoft.Robotics.Services.DepthCamSensor.DepthCamSensorState state) 
        {
            if(state.DepthImage == null)
            {
                _state.LogicalState = LogicalState.UnknownSurrondings;
                return;
            }            
            Bitmap bmp = MakeDepthBitmap(state.DepthImageSize.Width, state.DepthImageSize.Height, AdjustDepthPixels(state.DepthImage));            
            shapeClassifier.FindObjects(bmp, state.DepthImage, ref classified);            
            DisplayClassifiedObjects(classified);
            
            if (_state.RotationCount == 360 / _state.CameraAngle)
            {   
                _state.RotationCount = 0;                       
                _state.LogicalState = LogicalState.AdjustDirection;                
            }
            else
            {                           
                SpawnIterator(Turn);
            }           
            
        }        
        private IEnumerator<ITask> PollDepthFrame()
        {
            _state.LogicalState = LogicalState.Mapping;
            yield return this._depthCamSensorPort.Get().Choice(
                depthResult => { processDepthImage(depthResult); },
                failure => { LogError(failure); }
                );
        }
        #endregion
        #region ControlPanel
        //Function taken in it's entirety from the Microsoft Robotics Development Studio Documentation.
        //Function taken in it's entirety from the Microsoft Robotics Development Studio Documentation.
        private Bitmap MakeDepthBitmap(int width, int height, short[] depthData)
        {
            // NOTE: This code implicitly assumes that the width is a multiple
            // of four bytes because Bitmaps have to be longword aligned.
            // We really should look at bmp.Stride to see if there is any padding.
            // However, the width and height come from the webcam and most cameras
            // have resolutions that are multiples of four.

            const short MaxDepthDataValue = 4003;

            var buff = new byte[width * height * 3];
            var j = 0;
            for (var i = 0; i < width * height; i++)
            {
                // Convert the data to a suitable range
                byte val;
                if (depthData[i] >= MaxDepthDataValue)
                {
                    val = byte.MaxValue;
                }
                else
                {
                    val = (byte)((depthData[i] / (double)MaxDepthDataValue) * byte.MaxValue);
                }

                // Set all R, G and B values the same, i.e. gray scale
                buff[j++] = val;
                buff[j++] = val;
                buff[j++] = val;
            }

            // NOTE: Windows Forms do not support Format16bppGrayScale which would be the
            // ideal way to display the data. Instead it is converted to RGB with all the
            // color values the same, i.e. 8-bit gray scale.
            Bitmap bmp = null;

            try
            {
                bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);

                var data = bmp.LockBits(
                    new Rectangle(0, 0, bmp.Width, bmp.Height),
                    ImageLockMode.WriteOnly,
                    PixelFormat.Format24bppRgb);

                Marshal.Copy(buff, 0, data.Scan0, buff.Length);

                bmp.UnlockBits(data);
            }
            catch (Exception ex)
            {
                if (bmp != null)
                {
                    bmp.Dispose();
                    bmp = null;
                }

                LogError(ex);
            }

            return bmp;
        }
        /// <summary>
        ///  Displays the classified objects in a list in the control panel(Form).
        /// </summary>
        /// <param name="objects">Classified objects from a classifier</param>
        private void DisplayClassifiedObjects(List<ClassifiedObject> objects)
        {
            Fault fault = null;
            var displayImage = new FormInvoke(() => this.panel.DisplayClassifiedObjects(objects));
            WinFormsServicePort.Post(displayImage);
            Arbiter.Choice(displayImage.ResultPort, EmptyHandler, e => fault = Fault.FromException(e));
            if (fault != null)
            {
                LogError("Error : Unable to display information about last classified objects, make sure the panel is enabled");
            }
        }
        /// <summary>
        ///  Displays captured WebCamFrame.
        /// </summary>
        /// <param name="bp">Bitmap to display</param>
        private void DisplayWebcamFrame(Bitmap bp)
        {
            Fault fault = null;
            var displayImage = new FormInvoke(() => this.panel.WebCamFrame = bp);
            WinFormsServicePort.Post(displayImage);
            Arbiter.Choice(displayImage.ResultPort, EmptyHandler, e => fault = Fault.FromException(e));
            if (fault != null)
            {
                LogError("Error : Unable to update last captured webcamframe in controlpanel, make sure the panel is enabled");
            }
        }
        /// <summary>
        /// Displays captured DepthCamFrame.
        /// </summary>
        /// <param name="bp">Bitmap to display</param>
        private void DisplayDepthcamFrame(Bitmap bp)
        {
            Fault fault = null;
            var displayImage = new FormInvoke(() => this.panel.DepthCamFrame = bp);
            WinFormsServicePort.Post(displayImage);
            Arbiter.Choice(displayImage.ResultPort, EmptyHandler, e => fault = Fault.FromException(e));
            if (fault != null)
            {
                LogError("Error : Unable to update last captured depthcamframe in controlpanel, make sure the panel is enabled");
            }
        }
        private Form InitializeControlPanel()
        {
            this.panel = new ControlPanel();
            return this.panel;
        }
        #endregion
    }
}


