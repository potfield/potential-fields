using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;

namespace Navigation
{

    public enum Classification
    {
        GOAL,
        FOE,
        OBSTACLE
    }
    public enum Shapes
    {
        CIRCLE,
        BOX,
        SQUARE,
        TRIANGLE,
        CONE,
        SPHERE,
        WALL
    }
    public class ClassifiedObject
    {
        private int x;
        private int y;
        private int r;
        private Classification classification;
        public ClassifiedObject(int x, int y, int r, Classification classification)
        {
            this.x = x;
            this.y = y;
            this.r = r;
            this.classification = classification;
        }
        public int X
        {
            get { return this.x; }
            set { this.x = value; }
        }
        public int Y
        {
            get { return this.y; }
            set { this.y = value; }
        }
        public int R
        {
            get { return this.r; }
            set { this.r = value; }
        }
        public Classification Classification
        {
            get { return this.classification; }
            set { this.classification = value; }
        }
    }
	public sealed class Contract
	{
		[DataMember]
        public const string Identifier = "http://schemas.tempuri.org/2016/03/navigation.html";
	}

    [DataContract]
    public enum LogicalState
    {
        UnknownSurrondings,
        FreeForward,
        AdjustDirection,
        Mapping,
        TurnedOn,
        TaskCompleted,
        NoGoalInSight,
        Busy
    }
	
	[DataContract]
	public class NavigationState
	{
        private LogicalState state;
        private double headingOffset = 0.0;
        private double CAMERA_ANGLE = 60;
        private int rotationCount = 0;
        private const int HORIZONTAL_MIDDLE_INPX = 180;

        private bool seeDepth = true;
        private bool seeColor = false;
               
        [DataMember]
        public LogicalState LogicalState
        {
            get { return state; }
            set { state = value; }
        }
        //Getters
        internal double CameraAngle { get { return this.CAMERA_ANGLE; } }
        
        internal int RotationCount
        {
            get { return rotationCount; }
            set { this.rotationCount = value; }
        }
        internal double HeadingOffset 
        {
            get { return headingOffset; }
            set { headingOffset = value; }
        }        
        internal bool IsUnknown
        {
            get{ return LogicalState == LogicalState.UnknownSurrondings; }
        }        
        internal bool IsMoving 
        {
            get{ return !IsUnknown; }
        }
        internal bool PerformingTask
        {
            get{ return LogicalState != LogicalState.TaskCompleted; }
        }
        internal bool SeeDepth
        {
            get { return this.seeDepth; }
            set { this.seeDepth = value; }
        }
        internal bool SeeColor
        {
            get { return this.seeColor; }
            set { this.seeColor = value; }
        }
	}
	
	[ServicePort]
	public class NavigationOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get>
	{
	}
	
	public class Get : Get<GetRequestType, PortSet<NavigationState, Fault>>
	{
		public Get()
		{
		}
		
		public Get(GetRequestType body)
			: base(body)
		{
		}
		
		public Get(GetRequestType body, PortSet<NavigationState, Fault> responsePort)
			: base(body, responsePort)
		{
		}
	}
}


