﻿using Navigation.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Navigation
{ 
    /*Formula for calculations and more information can be found at:
     * http://people.csail.mit.edu/lpk/mars/temizer_2001/Potential_Field_Method/#Construction of the Repulsive Potential Field
     */

    /*The purpose of this class is to calculate the attractive potential field that PotentialFields class uses*/
    public class AttractivePF
    {
        public Point robotPos;
        public Point goalPos;
        public double radius;
        public double spread;
        public double alpha;
        public Point gradientVDelta;

        public AttractivePF(Point robotPos, Point goalPos, double radius, double spread)
        {
            this.robotPos = robotPos;
            this.goalPos = goalPos;
            this.radius = radius;
            this.spread = spread;
        }

        //Returns the gradient vector
        public Point seekGoal()
        {
            //Find the distance between the goal and the agent: d = sqrt((XG-x)^2 + (y-yG)^2)
            double dist = distance(robotPos, goalPos);
            //Find the angle between the agent and the goal Theta = tan^(-1) ((yG-y)/xG-x))
            double angl = angle();
            //Alpha is set to 1 as is explained in PotentialFields.cs
            alpha = 1;

            double xD = 0;
            double yD = 0;
            if (dist < radius)
            {
                xD = 0;
                yD = 0;
            }
            else if (radius <= dist && dist <= (spread + radius))
            {
                xD = (alpha * (dist - radius)) * Math.Cos(angl);
                yD = (alpha * (dist - radius)) * Math.Sin(angl);
            }
            else if (dist > (spread + radius))
            {
                xD = (alpha * spread) * Math.Cos(angl);
                yD = (alpha * spread) * Math.Sin(angl);
            }
            gradientVDelta.X = (int)xD;
            gradientVDelta.Y = (int)yD;
            return gradientVDelta;
        }
               
        //Helper function to calculate the distance between where the robot is and where the goal is
        public double distance(Point robotPos, Point pos)
        {            
            return (Math.Sqrt(Math.Pow((pos.X - robotPos.X), 2) + Math.Pow((robotPos.Y - pos.Y), 2)));
        }
        //Helper function to find the angle between the agent and the goal
        public double angle()
        {
            //First calculate the length of x1 to x2 (a) and y1 to y2 (b)
            double a = goalPos.X - robotPos.X;
            double b = goalPos.Y - robotPos.Y;
            //Second calculate angle B: (cos(alpha) = (b/c); B = arccos(b/a);)            
            double B = (Math.Atan2(b, a) * (180/Math.PI));
            return B;
        }
    }
}
