﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.Drawing;

using System.Diagnostics;
using Navigation.Utilities;
namespace Navigation
{   
    class Classifier 
    {        
        private const int TRIANGLE_VERT = 3;
        private const int BOX_VERT = 4;
        private const int MIN_RIGHTANGLE = 80;
        private const int MAX_RIGHTANGLE = 100;
        private const int HORIZONTAL_MIDDLE_INPX = 180;

        public void FindObjects(Bitmap bp, short[] depthData, ref List<ClassifiedObject> foundObjects)
        {
            Image<Gray, Byte> grayScale = new Image<Gray, byte>(bp).Convert<Gray, Byte>().SmoothMedian(3);
            FindAndClassifyObjects(grayScale, depthData, ref foundObjects);
        }
        private void FindAndClassifyObjects(Image<Gray, byte> grayScaledFrame, short[] depthData, ref List<ClassifiedObject> foundObjects)
        {            

            Image<Gray, byte> otsuGray = grayScaledFrame.CopyBlank();
            Image<Gray, byte> cannyEdges = grayScaledFrame.CopyBlank();

            //Create otsu threaasholds (i.e based on frame)
            double cannyThreshold = CvInvoke.cvThreshold(cannyEdges.Ptr, otsuGray.Ptr, 0, 255, Emgu.CV.CvEnum.THRESH.CV_THRESH_OTSU | Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY);
            double cannyThresholdLinking = 0.001 * cannyThreshold;
            otsuGray.Dispose();
            //Perform canny-edge detection with otsu threasholds.
            cannyEdges = grayScaledFrame.Canny(new Gray(cannyThreshold), new Gray(cannyThresholdLinking));

            //No circles to search for anyhow.
            //FindCircularObjects(grayScaledFrame, ref foundObjects, cannyThreshold, cannyThresholdLinking);
            FindCustomObjects(cannyEdges, ref foundObjects, depthData);
            
        }
        private void FindCircularObjects(Image<Gray, byte> grayScaled, ref List<ClassifiedObject> foundObjects, double cannyThreshold, double cannyThresholdLinking)
        {
            Classification circularClassification = Classify(Shapes.CIRCLE);
            CircleF[] circles = grayScaled.HoughCircles(new Gray(cannyThreshold), new Gray(cannyThresholdLinking), 5.0, 10.0, 5, 0)[0];
            for (int i = 0; i < circles.Length; i++)
            {
                foundObjects.Add(new ClassifiedObject((int)circles[i].Center.X, (int)circles[i].Center.Y, (int)circles[i].Radius, circularClassification));
            }
        }
        private void FindCustomObjects(Image<Gray, byte> cannyEdges, ref List<ClassifiedObject> foundObjects, short[] depthData)
        {
            Classification triangleClassification = Classify(Shapes.TRIANGLE);
            Classification wallClassification = Classify(Shapes.WALL);
            Classification boxClassification = Classify(Shapes.BOX);

            using (MemStorage mem = new MemStorage())
            {

                for (Contour<Point> contours = cannyEdges.FindContours(); contours != null; contours = contours.HNext)
                {
                    Contour<Point> currContours = contours.ApproxPoly(contours.Perimeter * 0.05, mem);
                    if (currContours.Total == TRIANGLE_VERT)
                    {                        
                        Point[] vertices = currContours.OrderBy(vertex => vertex.Y).ToArray();
                        int radius = Math.Abs(vertices[0].X - vertices[1].X) / 2;
                        if (vertices[0].X < vertices[1].X)
                        {
                            foundObjects.Add(new ClassifiedObject((vertices[0].X + radius) - HORIZONTAL_MIDDLE_INPX, ConvertDepthPixelToMM((vertices[0].X + radius),vertices[0].Y, depthData), radius, triangleClassification));
                            
                        }
                        else
                        {
                            foundObjects.Add(new ClassifiedObject((vertices[1].X + radius) - HORIZONTAL_MIDDLE_INPX, ConvertDepthPixelToMM((vertices[0].X + radius), vertices[1].Y, depthData), radius, triangleClassification));
                        }
                    }
                    else if (currContours.Total == BOX_VERT)
                    {
                        int lowX = -1;
                        int lowY = -1;
                        int currentX = 0;
                        int currentY = 0;
                        int longestLine = -1;
                        bool isRightAngle = true;

                        Point[] vertices = currContours.ToArray();
                        LineSegment2D[] edges = PointCollection.PolyLine(vertices, true);
                        for (int i = 0; i < edges.Length; i++)
                        {
                            double angle = Math.Abs(edges[(i + 1) % edges.Length].GetExteriorAngleDegree(edges[i]));
                            if (angle < MIN_RIGHTANGLE || angle > MAX_RIGHTANGLE)
                            {
                                isRightAngle = false;
                            }
                            if (longestLine == -1 || Math.Abs(edges[i].P1.X - edges[i].P2.X) > longestLine)
                            {
                                longestLine = Math.Abs(edges[i].P1.X - edges[i].P2.X);
                            }
                            currentX = Math.Min(edges[i].P1.X, edges[i].P2.X);
                            if (lowX == -1 || currentX < lowX)
                            {
                                lowX = currentX;
                            }
                            currentY = Math.Min(edges[i].P1.Y, edges[i].P2.Y);
                            if (lowY == -1 || currentY < lowY)
                            {
                                lowY = currentY;
                            }
                        }
                        foundObjects.Add(
                            new ClassifiedObject(
                                lowX + (longestLine / 2),
                                ConvertDepthPixelToMM(lowX, lowY, depthData),
                                longestLine / 2,
                                wallClassification));
                    }
                }
            }
        }
        private int ConvertDepthPixelToMM(int x, int y, short[] depthData)
        {
            return (int)depthData[(y * x) + x] >> 3;
        }
        private Classification Classify(Shapes shape)
        {
            Classification classification = Classification.GOAL;
            switch (shape)
            {
                case Shapes.WALL:
                    classification = Classification.OBSTACLE;
                    break;
                case Shapes.BOX:
                    classification = Classification.OBSTACLE;
                    break;
                case Shapes.TRIANGLE:
                    classification = Classification.GOAL;
                    break;
                default:
                    break;
            }
            return classification;
        }
    }
}
