﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Navigation.Utilities
{
    public class Position
    {
        public double x;
        public double y;

        public Position(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
