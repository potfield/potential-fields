﻿using Navigation.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Diagnostics;

namespace Navigation
{
    //Receives current position and list of objects that Classifier has classified coordinates, radius, what kind of object and if it is
    // a GOAL, OBSTACLE or FOE
    public class PotentialFields
    {
        /* Formula for calculating attractive field, reject field and combining them is taken from:
         * http://phoenix.goucher.edu/~jillz/cs325_robotics/goodrich_potential_fields.pdf
         * but the value of alpha, beta and spread are from http://people.csail.mit.edu/lpk/mars/temizer_2001/Potential_Field_Method/index.html
        */

        //Current position of the robot, if time it would be great to correct the position error that happens due to movement of the robot
        public Point currentPos;
        //List of potential field calculation of each classified object
        public List<Point> allFields;
        //The resulting potential field after summing the list of allFields
        public Point potField;
        //Radius of the object (obstacle or goal)
        public double radius; 
        //How far the potential field expands, we set it as 2.0 as is done in the step by step guide (see reference in the report)
        //Better would be to have this be decided by what kind of field is being calculated
        private double spread = 2.0;
        private bool goalInSight;

        public PotentialFields() { }
        public PotentialFields(Point currentPos, List<ClassifiedObject> objects)
        {
            this.currentPos = currentPos;
            ConstructFields(objects, currentPos);
        }
        public void ClearFields()
        {
            potField.X = 0;
            potField.Y = 0;
        }
        public void ConstructFields(List<ClassifiedObject> objects, Point currentPos)
        {
            this.currentPos = currentPos;
            bool goal = false;
            allFields = new List<Point>();

            foreach (ClassifiedObject classifiedO in objects)
            {
                switch (classifiedO.Classification)
                {
                    case Classification.GOAL:
                        //The artificial potential field, which the agent is subjected to
                        AttractivePF attField = new AttractivePF(currentPos, new Point(classifiedO.X, classifiedO.Y), (double)classifiedO.R, spread); 
                        //Attractive potential field calculated and added to the list of potential fields
                        allFields.Add(attField.seekGoal());
                        goal = true;
                        break;
                    /*Below is for repulsive field, with that field there is a lot more cases to consider, especially when 
                     * combining it with attractive field such as if goal is behind obstacle and so on. Due to that we have it commented out
                     */
                    /*
                    case Classification.OBSTACLE:
                        //Repulsive potential field created by the obstacle 
                        RejectPF rejField = new RejectPF(currentPos, classifiedO.R, spread);
                        Point obstacle = new Point(classifiedO.X, classifiedO.Y);
                        //Repulsive potential field calculated for the obstacle
                        allFields.Add(rejField.reject(obstacle));
                        break;
                     */
                    default:
                        Debug.WriteLine("Classifier not classifying correctly");
                        break;
                }
            }
            potField = calculatePFields();
            GoalInSight = goal;
        }
        /*This function would be used when implementing the robot to stop when the robot is at goal position*/
        public bool GoalInSight
        {
            get { return this.goalInSight; }
            set { this.goalInSight = value; }
        }
        //Sums all potential fields into one potential field
        public Point calculatePFields()
        {           
            Point tempPoint = new Point(0,0);
            foreach (Point field in allFields)
            {
                tempPoint.X += field.X;
                tempPoint.Y += field.Y;
            }
            int direction = angle(tempPoint);
            tempPoint.X = direction;
            return tempPoint;
        }

        /*Calculates the change in direction the robot should change*/
        public int angle(Point gradV)
        {
            //Calculate angle B: (cos(alpha) = (b/c); B = arccos(b/a);)
            double B = (Math.Atan2(gradV.Y, gradV.X));
            return (int)B;
        }

        /*Not implemented yet, this is the speed the robot should go*/
        public double velocity(Point gradV)
        {
            return 0.0;
        }
    }
}
