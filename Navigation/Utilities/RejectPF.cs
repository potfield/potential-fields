﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Navigation.Utilities
{
    /*The purpose of this class is to calculate repulsive fields such as for obstacles*/
    public class RejectPF
    { 
        public Point robotPos;
        public Point obstaclePos;
        public Point gradientPos;
        public double radius;
        public double spread;

        public RejectPF(Point robotPos, double radius, double spread)
        {
            this.robotPos = robotPos;
            this.radius = radius;
            this.spread = spread;
        }

        //Returns the gradient vector
        public Point reject(Point obstaclePos)
        {
            //Find the distance between the goal and the agent: d = sqrt((XG-x)^2 + (y-yG)^2)
            double dist = distance(robotPos, obstaclePos);
            //Find the angle between the agent and the goal Theta = tan^(-1) ((yG-y)/xG-x))
            double angl = angle();
            //Beta is set to 1 as is explained in PotentialFields.cs
            double beta = 1; 

            if (dist < radius)
            {
                gradientPos.X = (int)((-Math.Sign(Math.Cos(angl))*Double.PositiveInfinity));
                gradientPos.Y = (int)(-Math.Sign(Math.Sin(angl)) * Double.PositiveInfinity); 
            }
            else if (radius <= dist && dist <= (spread + radius))
            {
                gradientPos.X = (int)((-beta * (spread + radius - dist) * Math.Cos(angl)));
                gradientPos.Y = (int)(-beta * (spread + radius - dist) * Math.Sin(angl));
            }
            else if (dist > (spread + radius))
            {
                gradientPos.X = 0;
                gradientPos.Y = 0;
            }
            return gradientPos;
        }

        //Helper function to calculate the distance between where the robot is and where the obstacle is
        public double distance(Point robotPos, Point pos)
        {
            return (Math.Sqrt(Math.Pow((pos.X - robotPos.X), 2) + Math.Pow((pos.Y - robotPos.Y), 2)));
        }
        //Helper function to find the angle between the agent and the goal
        public double angle()
        {
            //First calculate the length of x1 to x2 (a) and y1 to y2 (b)
            double a = obstaclePos.X - robotPos.X;
            double b = obstaclePos.Y - robotPos.Y;
            //Second calculate angle B: (cos(alpha) = (b/c); B = arccos(b/a);)
            double B = (Math.Atan2(a, b) * (180 / Math.PI));
            return B;
        }
    }
}
