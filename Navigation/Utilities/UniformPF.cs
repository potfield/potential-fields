﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Navigation.Utilities
{
    //From http://phoenix.goucher.edu/~jillz/cs325_robotics/goodrich_potential_fields.pdf
    class UniformPF
    {
        //Is obtained by setting delta x = c and deltaY = 0; c is a constant value
	    //that represents the direction desired.
        //Move ahead (move forward, move along obstacle)
        //TODO: Add to PotentialFields.cs if time
        public Point forwardField(int c)
        {
            Point gradientV = new Point(0,0);
            gradientV.X = c;
            gradientV.Y = 0;
            return gradientV;
        }
    }
}
