﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Navigation;
using System.Drawing;

namespace NavigationTest
{
    [TestClass]
    public class AttractivePFTest
    {
        Point robotPos = new Point(0, 0);
        Point goalPos = new Point(8, 155);
        double radius = 8.0;
        double spread = 2.0;
        AttractivePF testField;

        [TestMethod]
        public void TestAttractiveDistance()
        {
            testField = new AttractivePF(robotPos, goalPos, radius, spread);
      
            double expected = 155.21;
            double actual = testField.distance(testField.robotPos, testField.goalPos);

            Assert.AreEqual(expected, actual, 0.01, "Distance not calculated correctly");
        }

        [TestMethod]
        public void TestAttractiveAngle()
        {
            testField = new AttractivePF(robotPos, goalPos, radius, spread);
            double expected = 87.05;
            double actual = testField.angle();
            Assert.AreEqual(expected, actual, 0.01, "Angle not correct");
        }

        [TestMethod]
        public void TestAttractiveSeekFirstIf()
        {
            testField = new AttractivePF(robotPos, goalPos, 200, spread);
            Point expected = new Point(0, 0);
            Point actual = testField.seekGoal();

            Assert.AreEqual(expected.X, actual.X, 0.01, "1. Something wrong with dist or radius calc (X)");
            Assert.AreEqual(expected.Y, actual.Y, 0.01, "1. Something wrong with dist or radius calc (Y)");
        }

        [TestMethod]
        public void TestAttractiveSeekSecondIf()
        {
            testField = new AttractivePF(robotPos, goalPos, 8, 200);
            Point expected = new Point(89, -117);
            Point actual = testField.seekGoal();

            Assert.AreEqual(expected.X, actual.X, "2. Something wrong with dist or radius calc (X)");
            Assert.AreEqual(expected.Y, actual.Y, "2. Something wrong with dist or radius calc (Y)");
        }

        [TestMethod]
        public void TestAttractiveSeekThirdIf()
        {
            testField = new AttractivePF(robotPos, goalPos, 8, spread);
            Point expected = new Point(1,-1);
            Point actual = testField.seekGoal();
            Assert.AreEqual(expected.X, actual.X, 0.01, "3. Something wrong with dist or radius calc (X)");
            Assert.AreEqual(expected.Y, actual.Y, 0.01, "3. Something wrong with dist or radius calc (Y)");
        }
    }
}
