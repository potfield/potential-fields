﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using Navigation.Utilities;
using Navigation;
using System;
using System.Collections.Generic;


namespace NavigationTest
{
    [TestClass]
    public class PotentialFieldTest
    {
        Point robotPos = new Point(0, 0);
        Point goalPos = new Point(8, 162);
        //Point obstaclePos = new Point(3, 805);
        double radius = 8.0;
        double spread = 2.0;
        AttractivePF testAF;
        RejectPF testRF;
        
        [TestMethod]
        public void TestCalculatePF()
        {
            PotentialFields testPF = new PotentialFields();
            List<Point> allFields = new List<Point>();
            List<ClassifiedObject> objects = new List<ClassifiedObject>();
            ClassifiedObject goal = new ClassifiedObject(8, 155, 8, Classification.GOAL);
            objects.Add(goal);

            testAF = new AttractivePF(robotPos, goalPos, radius, spread);
            testRF = new RejectPF(robotPos, radius, spread);
            allFields.Add(testAF.seekGoal());
            //allFields.Add(testRF.reject(obstaclePos));
            //Expected for x is -0.79 which should be -1 instead of 0, could be bad calculation by a human agent
            Point expected = new Point(-1,0); 
            testPF.ConstructFields(objects, (new Point(0,0)));
            Assert.AreEqual(expected.X, testPF.potField.X, "error in calculation (x)");
            //Not yet implemented in the class
            //Assert.AreEqual(expected.Y, testPF.potField.Y, "Error in calculating pf (y)"); 
        }
    }
}
