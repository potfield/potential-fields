﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using Navigation.Utilities;

namespace NavigationTest
{
    [TestClass]
    public class RejectPFTest
    {
        Point robotPos = new Point(0, 800);
        Point obstaclePos = new Point(3, 805);
        double radius = 2.0;
        RejectPF testField;
        public int spread = 2;

        [TestMethod]
        public void TestDistanceReject()
        {
            testField = new RejectPF(robotPos, radius, spread);
            testField.obstaclePos = obstaclePos;
            double expected = 5.83;

            double actual = testField.distance(testField.robotPos, obstaclePos);
            Assert.AreEqual(expected, actual, 0.001, "Distance not calculated correctly");
        }
        [TestMethod]
        public void TestAngleReject()
        {

            testField = new RejectPF(robotPos, radius, spread);
            testField.obstaclePos = obstaclePos;
            double expected = 30.96;
            double actual = testField.angle();
            Assert.AreEqual(expected, actual, 0.01, "Angle not correct");
        }

        [TestMethod]
        public void TestRejectFirstIf()
        {
            //int beta = 1;
            
            testField = new RejectPF(robotPos, 6, spread);
            testField.obstaclePos = obstaclePos;
            Point expected = new Point(-2147483648, -2147483648);
            Point actual = testField.reject(obstaclePos);
            
            Assert.AreEqual(expected.X, actual.X, 0.01, "1. Something wrong with reject (X)");
            Assert.AreEqual(expected.Y, actual.Y, 0.01, "1. Something wrong with reject (Y)");
        }

        [TestMethod]
        public void TestRejectSecondIf()
        {
            //int beta = 1;
            int spread = 3;
            testField = new RejectPF(robotPos, spread, 100);
            testField.obstaclePos = obstaclePos;
            Point expected = new Point(-87, 42);
            Point actual = testField.reject(obstaclePos);
            Assert.AreEqual(expected.X, actual.X, 0.01, "1. Something wrong with reject (X)");
            Assert.AreEqual(expected.Y, actual.Y, 0.01, "1. Something wrong with reject (Y)");
   
        }

        [TestMethod]
        public void TestRejectThirdIf()
        {
            testField = new RejectPF(robotPos, 2, spread);
            Point expected = new Point(0, 0);
            Point actual = testField.reject(obstaclePos);
            Assert.AreEqual(expected.X, actual.X, "3. Something wrong with reject (X)");
            Assert.AreEqual(expected.Y, actual.Y, "3. Something wrong with reject (Y)");

        }
    }
}
